let employeeOne = {
    name: "John",
    email: "john@mail.com",
    sales: [89, 84, 78, 88],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listSales(){
        console.log(`${this.name} has quarterly sales of ${this.sales}`)
    },
    computeAve(){
        let sum = 0
        
        this.sales.forEach(indiv_sale => sum += indiv_sale)
        return sum/4;
    },
    hasMetQouta() {
        return this.computeAve() >= 85 ? true : false
    },
    willGetBonus(){
        return (this.computeAve() && this.computeAve() >= 90) ? true : false
    }
};

let employeeTwo = {
    name: "Joe",
    email: "joe@mail.com",
    sales: [78, 82, 79, 85],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listSales(){
        console.log(`${this.name} has quarterly sales of ${this.sales}`)
    },
    computeAve(){
        let sum = 0
        
        this.sales.forEach(indiv_sale => sum += indiv_sale)
        return sum/4;
    },
    hasMetQouta() {
        return this.computeAve() >= 85 ? true : false
    },
    willGetBonus(){
        return (this.computeAve() && this.computeAve() >= 90) ? true : false
    }

};
let employeeThree = {
    name: "Jane",
    email: "jane@mail.com",
    sales: [87, 89, 91, 93],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listSales(){
        console.log(`${this.name} has quarterly sales of ${this.sales}`)
    },
    computeAve(){
        let sum = 0
        
        this.sales.forEach(indiv_sale => sum += indiv_sale)
        return sum/4;
    },
    hasMetQouta() {
        return this.computeAve() >= 85 ? true : false
    },
    willGetBonus(){
        return (this.computeAve() && this.computeAve() >= 90) ? true : false
    }
};

let employeeFour = {
    name: "Jessica",
    email: "jessica@mail.com",
    sales: [91, 89, 92, 93],
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },
    listSales(){
        console.log(`${this.name} has quarterly sales of ${this.sales}`)
    },
    computeAve(){
        let sum = 0
        
        this.sales.forEach(indiv_sale => sum += indiv_sale)
        return sum/4;
    },
    hasMetQouta() {
        return this.computeAve() >= 85 ? true : false
    },
    willGetBonus(){
        return (this.computeAve() && this.computeAve() >= 90) ? true : false
    }
};

let teamLead = {
    name: "bob",
    email: "bob@gmail.com",
    country: "Germany",
    team: "Sales Support",
    tenure: 3,
    login(){
        console.log(`${this.email} has logged in`)
    },
    logout(){
        console.log(`${this.email} has logged out`)
    },

    getCountryAndTeam() {
        console.log(`${this.name} works in ${this.country}'s as ${this.team}`)
    },

    increaseTenure(numOfYear){
        this.tenure += numOfYear;

    },

    getTenure(){
        console.log(`${this.name} has worked for ${this.tenure} years in the company`)
    }
}

teamLead.login()

teamLead.getCountryAndTeam()
teamLead.getTenure()
console.log("Number of year and tenure is " + teamLead.increaseTenure(5))
teamLead.getTenure()
teamLead.logout()

const teamBob = {
    lead: teamLead,
    employees: [employeeOne, employeeTwo, employeeThree, employeeFour],
    countBonusEarners() {
        let count = 0
        for( i=0;i<this.employees.length; i++){
            if(this.employees[i].willGetBonus()){
                count++;
            }
        }
        return count
    },
    getBonusEarnerPercentage() {
        let per = (this.countBonusEarners() / this.employees.length) * 100
        
        console.log(`${per}% of the team will receive bonus this year`)
    }

}
console.log(teamBob.lead.country)
console.log(teamBob.employees[1].computeAve())
console.log(teamBob.countBonusEarners())
teamBob.getBonusEarnerPercentage()